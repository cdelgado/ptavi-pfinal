#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Programa para el proxy
"""

import socketserver
import sys
import time
import json
import socket
import os.path as path
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import log


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        self.list = []
        self.log = ""
        self.inlog = False
        self.listattr = [["name", "ip", "puerto"],
                         ["path", "passwdpath"]]

    def startElement(self, name, attrs):
        list_atr = []
        if name == 'server':
            list_atr = [name]
            for attr in self.listattr[0]:
                list_atr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_atr)
        elif name == 'database':
            list_atr = [name]
            for attr in self.listattr[1]:
                list_atr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_atr)

        elif name == 'log':
            self.inlog = True

    def characters(self, content):
        if self.inlog:
            self.log += content

    def get_log(self):
        return self.log

    def get_tags(self):
        return self.list


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    users = {}

    def register2json(self):
        """
        choose a json file and add the users
        """
        with open(database_path, "w") as outfile:
            json.dump(self.users, outfile, indent=1)

    def logout(self):
        """
        create a list of logouts for delete and delete
        all the users with expires = 0 or expires < now
        """
        logout = []
        tiempo = time.time()
        if self.users[self.user]["expires"] == 0 or \
                self.users[self.user]["expires"] < 0:
            logout.append(self.user)
        for login in self.users:
            if self.users[self.user]["expires"] < tiempo:
                logout.append(login)
        for login in logout:
            del self.users[login]

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        client_msg = self.rfile.read().decode('utf-8')
        msg = client_msg.split(" ")
        log(log_path, "Received From ", self.client_address[0],
            self.client_address[1], client_msg)
        if msg[0] == "REGISTER":
            user2 = {}
            user2["ip"] = self.client_address[0]
            user2["port"] = msg[1].split(":")[2]
            user2["register_time"] = int(time.time())
            user2["expires"] = int(msg[3].split("\r\n")[0])
            + int(time.time())
            self.users[self.user] = user2
            self.logout()
            self.register2json()
            log(log_path, 'Sent To ', user2["ip"],
                user2["port"], "SIP/2.0 200 OK\r\n\r\n")
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        elif msg[0] == "BYE" or msg[0] == "ACK" or msg[0] == "INVITE":
            msg2 = msg[1].split(":")[1]
            if msg2 not in self.users:
                self.wfile.write(b"SIP/2.0 404 User Not Found\r\n\r\n")
                log(log_path, "Sent To ", self.client_address[0],
                    self.client_address[1],
                    "SIP/2.0 404 User Not Found\r\n\r\n")
            else:
                try:
                    guest_ip = self.users[guest]["ip"]
                    guest_port = int(self.users[guest]["port"])
                    with socket.socket(socket.AF_INET,
                                       socket.SOCK_DGRAM) as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)
                        my_socket.connect((guest_ip, guest_port))
                        print("Enviando: " + client_msg)
                        log(log_path, "Sent To ",
                            guest_ip, guest_port, client_msg)
                        my_socket.send(bytes(client_msg, 'utf-8'))
                        data = my_socket.recv(1024)
                        log(log_path, "Received From ", guest_ip,
                            guest_port, data.decode('utf-8'))
                        print('Recibido -- ', data.decode('utf-8'))
                        log(log_path, "Sent To ", self.client_address[0],
                            self.client_address[1], data.decode('utf-8'))
                        self.wfile.write(data)
                except ConnectionRefusedError:
                    self.wfile.write(bytes("Error: No server listening ",
                                     'utf-8'))
                    log(log_path, "Sent To ", self.client_address[0],
                        self.client_address[1], "Error: No server listening ")

    def json2registered(self):
        """
        search for a json file to load and if can't find it pass
        """
        try:
            if path.exists(database_path):
                with open(database_path, "r") as data_file:
                    self.users = json.load(data_file)
        except Exception:
            pass


if __name__ == "__main__":
    try:
        if len(sys.argv) != 2:
            sys.exit("Usage: python3 proxy_registrar.py config")
        else:
            parser = make_parser()
            sHandler = SmallSMILHandler()
            parser.setContentHandler(sHandler)
            parser.parse(open(sys.argv[1]))

            name = sHandler.get_tags()[0][1].split("=")[1]
            server = sHandler.get_tags()[0][2].split("=")[1]
            port = int(sHandler.get_tags()[0][3].split("=")[1])
            database_path = sHandler.get_tags()[1][1].split("=")[1]
            passwd_path = sHandler.get_tags()[1][2].split("=")[1]
            log_path = sHandler.get_log()
    except FileNotFoundError:
        sys.exit("Usage: python3 proxy_registrar.py config")

    serv = socketserver.UDPServer((server, port), EchoHandler)
    print(name + " listening at port " + str(port) + " ...")
    with open(log_path, "a") as outfile:
        outfile.write(time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) +
                      " Starting..." + "\r\n")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        with open(log_path, "a") as outfile:
            outfile.write(time.strftime('%Y%m%d%H%M%S',
                                        time.gmtime(time.time())) +
                          " Finishing..." + "\r\n")
