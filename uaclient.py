#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
import random
import simplertp
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def log(path, msg2, ip, port, msg):
    msg_log = '\r\n' + time.strftime('%Y%m%d%H%M%S',
                                     time.gmtime(time.time())) + " " + \
              msg2 + ip + ":" + str(port) + \
              " " + msg.replace('\r\n', ' ')
    with open(path, "a") as outfile:
        outfile.write(msg_log)


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.list = []
        self.log = ""
        self.inlog = False
        self.audio = ""
        self.inaudio = False
        self.listattr = [["username", "passwd"],
                         ["ip", "puerto"],
                         ["puerto"],
                         ["ip", "puerto"]]

    def startElement(self, name, attrs):
        list_attr = []
        if name == 'account':
            list_attr = [name]
            for attr in self.listattr[0]:
                list_attr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_attr)
        elif name == 'uaserver':
            list_attr = [name]
            for attr in self.listattr[1]:
                list_attr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_attr)
        elif name == 'rtpaudio':
            list_attr = [name]
            for attr in self.listattr[2]:
                list_attr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_attr)
        elif name == 'regproxy':
            list_attr = [name]
            for attr in self.listattr[3]:
                list_attr.append(attr + "=" + attrs.get(attr, ""))
            self.list.append(list_attr)
        elif name == 'log':
            self.inlog = True
        elif name == 'audio':
            self.inaudio = True

    def characters(self, content):
        if self.inlog:
            self.log += content
        if self.inaudio:
            self.audio += content

    def get_audio(self):
        return self.audio

    def get_log(self):
        return self.log

    def get_tags(self):
        return self.list


if __name__ == '__main__':
    try:
        METHOD = sys.argv[2]
        OPTION = sys.argv[3]
    except ValueError:
        sys.exit("Usage: python3 uaclient.py config metodo opcion")
    except IndexError:
        sys.exit("Usage: python3 uaclient.py config metodo opcion")
    except FileNotFoundError:
        sys.exit("Usage: python3 uaclient.py config metodo opcion")

    if len(sys.argv) != 4:
        sys.exit('Usage: python3 uaclient.py config metodo opcion')

    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open(sys.argv[1]))

    user = sHandler.get_tags()[0][1].split("=")[1]
    audio_port = int(sHandler.get_tags()[2][1].split("=")[1])
    proxy = sHandler.get_tags()[3][1].split("=")[1]
    proxy_port = int(sHandler.get_tags()[3][2].split("=")[1])
    log_path = sHandler.get_log().split(' ')[0]
    audio_path = sHandler.get_audio()

    if METHOD == "REGISTER":
        msg = METHOD + " sip:" + user + \
              " SIP/2.0\r\n" + "Expires: " + str(OPTION) + "\r\n\r\n"
    elif METHOD == "INVITE":
        body = "v=0\r\n" + "o=" + user.split(":")[0] + \
                      " " + proxy + "\r\n" + \
                      "s=missesion\r\n" + "t=0\r\n" + \
                      "m=audio " + str(audio_port) + " RTP\r\n"
        length = str(len(bytes(body, 'utf-8')))
        cabecera_proxy = 'VIA:SIP/2.0 ' + str(proxy) + ':' + str(proxy_port)
        msg = METHOD + " sip:" + OPTION + " SIP/2.0\r\n" + \
            "Content-Type: application/sdp\r\n" + \
            "Content-Length: " + length + '\r\n' + cabecera_proxy + \
            '\r\n' + body + '\r\n'
    elif METHOD == "BYE":
        msg = METHOD + " sip:" + OPTION + " SIP/2.0\r\n\r\n"

    # Creamos el socket
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxy, proxy_port))
        print("Enviando: " + msg)
        log(log_path, "Sent To ", proxy, proxy_port, msg)
        my_socket.send(bytes(msg, 'utf-8'))
        try:
            data = my_socket.recv(1024)
            print('Recibido -- ', data.decode('utf-8'))
            log(log_path, "Received From ", proxy,
                proxy_port, data.decode('utf-8'))
            if data.decode('utf-8').startswith("SIP/2.0 100 trying\r\n\r\n"
                                               "SIP/2.0 180 ringing"
                                               "\r\n\r\nSIP/2.0 200 OK\r\n"):
                msg = "ACK sip:" + OPTION + " SIP/2.0\r\n\r\n"
                my_socket.send(bytes(msg, 'utf-8'))
                log(log_path, "Sent To ", proxy, proxy_port, msg)

                aleat = random.randint(0, 99999)
                csrc = []
                aleat_csrc = random.randint(0, 15)
                for i in range(aleat_csrc):
                    csrc.append(aleat_csrc)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(pad_flag=0, ext_flag=0,
                                      cc=len(csrc), marker=0, ssrc=aleat)
                RTP_header.setCSRC(csrc)
                audioRTP = simplertp.RtpPayloadMp3(audio_path.split('\n')[0])
                simplertp.send_rtp_packet(RTP_header,
                                          audioRTP, '127.0.0.1', 5555)
                log(log_path, "Sent Audio To ",
                    '127.0.0.1', 5555, audio_path)
            print("Terminando socket...")
        except ConnectionRefusedError:
            log(log_path, "Error: No server listening at  ",
                proxy, proxy_port, "")
            sys.exit(str(time.time()) + "Error: No server listening at " +
                     proxy + " port " + str(proxy_port))
