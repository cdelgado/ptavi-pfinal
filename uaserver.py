#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import time
import simplertp
import random
from xml.sax import make_parser
from uaclient import SmallSMILHandler
from uaclient import log


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    dic_aux = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        client_msg = self.rfile.read().decode('utf-8')
        msg = client_msg.split(" ")
        if msg[1][:4] != "sip:":
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            log(log_path, "Received From ", self.client_address[0],
                self.client_address[1], client_msg)
            log(log_path, "Sent To ", self.client_address[0],
                self.client_address[1], "SIP/2.0 400 Bad Request\r\n")
        else:
            if msg[0] == "INVITE":
                user2 = client_msg.split("\r\n")[5]
                rtp2 = client_msg.split("\r\n")[8]
                ip2 = user2.split(' ')[1]
                port2 = rtp2.split(' ')[1]
                log(log_path, "Received From ", ip2, port2, client_msg)
                self.wfile.write(b"SIP/2.0 100 trying\r\n\r\nSIP/2.0 180 "
                                 b"ringing\r\n\r\nSIP/2.0 200 OK\r\n")
                body = "v=0\r\n" + "o=" + user.split(":")[0] + \
                       " " + server + "\r\n" + \
                       "s=missesion\r\n" + "t=0\r\n" + \
                       "m=audio " + str(audio_port) + " RTP\r\n"
                length = str(len(bytes(body, 'utf-8'))) + "\r\n\r\n"
                self.wfile.write(bytes(length, 'utf-8'))
                self.wfile.write(bytes(body, 'utf-8'))
                msg_log = "SIP/2.0 100 trying\r\n\r\nSIP/2.0 180 ringing" \
                          "\r\n\r\nSIP/2.0 200 OK\r\n" \
                          "Content-Type: application/sdp\r\nContent_Length:"\
                          + length + body
                log(log_path, "Received From ", ip2, port2, msg_log)
                self.dic_aux["ip"] = msg[5].split("\r\n")[0]
                self.dic_aux["port"] = int(msg[-2])
            elif msg[0] == "BYE":
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                log(log_path, "Received From ", self.client_address[0],
                    self.client_address[1], client_msg)
                log(log_path, "Sent To ", self.client_address[0],
                    self.client_address[1], "SIP/2.0 200 OK\r\n\r\n")
            elif msg[0] == "ACK":
                log(log_path, "Received From ", self.dic_aux["ip"],
                    self.dic_aux["port"], client_msg)
                aleat = random.randint(0, 99999)
                csrc = []
                aleat_csrc = random.randint(0, 15)
                for i in range(aleat_csrc):
                    csrc.append(aleat_csrc)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(pad_flag=0, ext_flag=0,
                                      cc=len(csrc), marker=0, ssrc=aleat)
                RTP_header.setCSRC(csrc)
                audioRTP = simplertp.RtpPayloadMp3(audio_path.split('\n')[0])
                simplertp.send_rtp_packet(RTP_header,
                                          audioRTP, '127.0.0.1',
                                          '5555')
                log(log_path, "Sent To ", '127.0.0.1',
                    '5555', audio_path)
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
                log(log_path, "Received From ", self.client_address[0],
                    self.client_address[1], client_msg)
                log(log_path, "Sent To ", self.client_address[0],
                    self.client_address[1],
                    "SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        if len(sys.argv) != 2:
            sys.exit("Usage: python3 uaserver.py config")
        else:
            parser = make_parser()
            sHandler = SmallSMILHandler()
            parser.setContentHandler(sHandler)
            parser.parse(open(sys.argv[1]))

            server = sHandler.get_tags()[1][1].split("=")[1]
            port = int(sHandler.get_tags()[1][2].split("=")[1])
            user = sHandler.get_tags()[0][1].split("=")[1]
            audio_port = int(sHandler.get_tags()[2][1].split("=")[1])
            log_path = sHandler.get_log().split(' ')[0]
            audio_path = sHandler.get_audio()
    except IndexError:
        sys.exit("Usage: python3 uaserver.py config")

    serv = socketserver.UDPServer((server, port), EchoHandler)
    print("Listening...")
    with open(log_path, "a") as outfile:
        tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
        outfile.write(tiempo + " Starting..." + "\r\n")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        with open(log_path, "a") as outfile:
            tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
            outfile.write(tiempo + " Finishing..." + "\r\n")
        sys.exit("Finish")
